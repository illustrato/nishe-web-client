# Nishe
Virtual Shopping Mall

## Description
Repo defines a VueJS (JavaScript Library) web application, interfacing the Nishe RESTful API to [Nishe](https://api.nishe.store), a virtual shopping mall, and an E-Commerce **"Module"** to the Phiscal business information system.
Application is licensed under [the GNU Public License version 3](https://www.gnu.org/licenses/gpl-3.0.txt).

## Dependencies
* Node Package Manager (npm)
* Active Nishe RESTful API

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
