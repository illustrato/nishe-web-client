'use strict';

import Api from '@/core/service/api';
import Http from '@/asset/js/logic/http';

// action types
export const NATIONS = 'getNations';

const actions = {
  [NATIONS]({}, target = 'object') {
    return new Promise((resolve, reject) => {
      const fx = function ({ payload }) {
        let list;
        switch (target) {
          case 'object':
            list = {};
            for (const item of payload) {
              list[item.id] = item.name;
            }
            break;
          case 'array':
            list = [];
            for (const item of payload) {
              list.push({ text: item.name, value: item.id });
            }
            break;
        }
        resolve(list);
      };
      const helper = new Http(fx, reject);
      Api.rmHeaders();
      Api.get(`${process.env.VUE_APP_PARENT}/index/nations`).then(helper.followup).catch(helper.error);
    });
  }
};

export default {
  namespaced: true,
  actions
};
