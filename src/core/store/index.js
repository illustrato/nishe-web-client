import Vue from 'vue';
import Vuex from 'vuex';

import generic from './generic.module';
import session from './rest/session.module';
import breadcrumbs from './template/breadcrumbs.module';
import config from './template/config.module';
import htmlclass from './template/htmlclass.module';
import industry from './rest/industry.module';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  modules: { breadcrumbs, config, generic, htmlclass, industry, session }
});
