'use strict';

import Http from '@/asset/js/logic/http';
import Api from '@/core/service/api';
import { cacheItem, getCache, getItem, saveItem, destroyItem } from '@/core/service/storage';
import { router, isNavigationFailure, NavigationFailureType } from '@/router';

// action types
export const AUTH = 'userAuthentication';
export const SIGNUP = 'register';
export const RM_USER = 'rmUser';
export const SIGNOUT = 'deletetoken';
export const VERIFY = 'verify';

// mutation types
export const CACHE_TENANT = 'setTenantId';
export const PURGE_AUTH = 'logOut';
export const SET_AUTH = 'setUser';
export const SET_AVATAR = 'setAvatar';
export const SET_BUSY = 'setPageBusy';
export const SET_FEEDS = 'setNotifications';
export const SET_TENANT = 'setCurrentTenant';
export const SET_SESSION = 'setDetails';

const state = {
  authenticated: !!getItem('JWT'),
  busy: false,
  feeds: { alert: [], event: [], log: [], unread: 0 },
  rank: {},
  tenant: false,
  user: { tenants: [] }
};

const getters = {
  /**
   * Determine if there's a user logged in
   * @param state
   * @returns {bool}
   */
  authenticated: function (state) {
    return state.authenticated;
  },

  /**
   * Rank of Current User to current Organization
   * @param state
   * @returns {string}
   */
  currentRank: function (state) {
    return state.rank;
  },

  /**
   * Tenant of current session
   * @param state
   * @returns {object}
   */
  currentTenant: function (state) {
    return state.tenant;
  },

  /**
   * User Currently logged in
   * @param state
   * @returns {object}
   */
  currentUser: function (state) {
    return state.user;
  },

  /**
   * URL to user Avatar, or path to placeholder if user has no Avatar
   * @param state
   * @returns {string}
   */
  getAvatar: function (state) {
    let pic = state.user.avatar || false;
    return pic === false ? 'asset/img/user/blank.png' : `${process.env.VUE_APP_FILES}/img/avatar/${state.user.id}.${state.user.avatar}`;
  },

  /**
   * List of User Notifications
   * @param state
   * @returns {array}
   */
  notifications: function (state) {
    return state.feeds;
  },

  /**
   * Determine if there are pending Processes
   * @param state
   * @returns {bool}
   */
  pageBusy: function (state) {
    return state.busy;
  }
};

const actions = {
  [AUTH]({ commit }, form) {
    return new Promise((resolve, reject) => {
      const fx = function ({ payload }) {
        commit(SET_AUTH, payload);
        resolve(payload);
      };
      const helper = new Http(fx, reject);
      Api.post(`${process.env.VUE_APP_PARENT}/session/auth`, form).then(helper.followup).catch(helper.error);
    });
  },
  [RM_USER]({ state }) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.delete(`${process.env.VUE_APP_PARENT}/user/${state.user.email}`).then(helper.followup).catch(helper.error);
    });
  },
  [SIGNOUT]({ commit }) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject, 200, (data) => {
        commit(PURGE_AUTH);
      });
      Api.setBearer();
      Api.delete(`${process.env.VUE_APP_PARENT}/session/${SIGNOUT}`).then(helper.followup).catch(helper.error);
    });
  },
  [SIGNUP]({}, form) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      form.firstname = form.firstname.trim();
      form.lastname = form.lastname.trim();
      Api.post(`${process.env.VUE_APP_PARENT}/session/register`, { ...form, module: 'N1' })
        .then(helper.followup)
        .catch(helper.error);
    });
  },
  [VERIFY]({ commit }) {
    if (getItem('JWT')) {
      const promise = new Promise((resolve, reject) => {
        const helper = new Http(resolve, reject);
        Api.setBearer();
        Api.setHeader('Tenant', getCache('tenant'));
        Api.get(`${process.env.VUE_APP_PARENT}/session/verify`).then(helper.followup).catch(helper.error);
      });
      promise
        .then(({ payload }) => {
          commit(SET_SESSION, payload);
          commit(SET_FEEDS, payload);
        })
        .catch((ex) => {
          commit(PURGE_AUTH);
          router.push({ name: 'signin', params: { errors: ex } }).catch((err) => {
            if (isNavigationFailure(err, NavigationFailureType.duplicated)) {
              window.alert(ex);
              window.alert(`${err.from.path} to ${err.to.path}`);
            }
          });
        });
      return promise;
    } else {
      commit(PURGE_AUTH);
      return { payload: { tenant: false, rank: { name: 'visitor' } } };
    }
  }
};

const mutations = {
  [SET_AUTH](state, payload) {
    saveItem('JWT', payload);
    state.authenticated = !!getItem('JWT');
  },
  [CACHE_TENANT](state, payload) {
    cacheItem('tenant', payload);
  },
  [SET_BUSY](state, payload) {
    state.busy = payload;
  },
  [SET_TENANT](state, payload) {
    state.tenant = payload;
  },
  [PURGE_AUTH](state) {
    destroyItem('JWT');
    state.user = { firstname: '', fullname: 'Guest User', email: '', profile: '', avatar: null, tenants: '[]' };
    state.tenant = { id: null, name: null, reg: null, web: null, motto: null, logo: null };
    state.jwt = null;
    state.authenticated = !!getItem('JWT');
  },
  [SET_SESSION](state, { user, tenant, rank }) {
    if (tenant === false && user.tenants.length === 1) {
      cacheItem('tenant', user.tenants[0]);
    }
    state.user = user;
    state.rank = rank;
    state.tenant = tenant;
  },
  [SET_FEEDS](state, { feeds }) {
    let notifications = { alert: [], event: [], log: [], unread: 0 };
    notifications.unread = feeds.unread;
    for (let i of feeds.records) {
      notifications[i.type].push(i);
    }
    state.feeds = notifications;
  },
  [SET_AVATAR](state, payload) {
    state.user.avatar = payload;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
