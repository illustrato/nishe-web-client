'use strict';

import Http from '@/asset/js/logic/http';
import { bool } from '@/asset/js/logic/hack';
import Api from '@/core/service/api';
import { getCache } from '@/core/service/storage';

// action types
export const ASSOCIATE = 'associate';
export const BANNER = 'banner';
export const DATA_INDUSTRY = 'industryData';
export const DISCONNECT = 'disconnect';
export const LS_INDUSTRY = 'industryList';

// mutation types
export const SET_CATEGORY = 'setCategoryId';
export const SET_INDUSTRY = 'setIndusrty';
export const SET_DESCRIPTION = 'setDescription';
export const SET_MAIN = 'setMain';
export const SET_DEALINGS = 'setDealings';
export const SET_DATA = 'setData';

const state = {
  data: {},
  dealings: []
};

const getters = {
  getDealings: function (state) {
    return state.dealings;
  }
};

const actions = {
  [ASSOCIATE]({ state }, banner) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.post(`/industry/${ASSOCIATE}`, { ...state.data, ...banner })
        .then(helper.followup)
        .catch(helper.error);
    });
  },
  [BANNER]({ state }) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.delete(`/industry/${BANNER}/${state.data.category_id}/${state.data.count}`).then(helper.followup).catch(helper.error);
    });
  },
  [DATA_INDUSTRY]({}, { categoryId, count }) {
    return new Promise((resolve, reject) => {
      const fx = ({ payload }) => {
        if (payload === false) {
          resolve(false);
          return;
        }
        const { banner, category_id, count, description, main } = payload;
        resolve({ banner: banner, category_id: category_id, count: parseInt(count), description: description, main: bool(main) });
      };
      const helper = new Http(fx, reject);
      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.get(`/industry/data/${categoryId}/${count}`).then(helper.followup).catch(helper.error);
    });
  },
  [DISCONNECT]({}, { categoryId, count }) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.setBearer();
      Api.setHeader('Tenant', getCache('tenant'));
      Api.delete(`/industry/${DISCONNECT}/${categoryId}/${count}`).then(helper.followup).catch(helper.error);
    });
  },
  [LS_INDUSTRY]({}, { target, tenantId }) {
    return new Promise((resolve, reject) => {
      const helper = new Http(resolve, reject);
      Api.rmHeaders();
      Api.get(`/industry/ls/${target}/${tenantId}`).then(helper.followup).catch(helper.error);
    });
  }
};

const mutations = {
  [SET_CATEGORY](state, payload) {
    state.data.category_id = payload;
  },
  [SET_INDUSTRY](state, payload) {
    state.data.count = payload;
  },
  [SET_DESCRIPTION](state, payload) {
    state.data.description = payload;
  },
  [SET_MAIN](state, payload) {
    state.data.main = payload;
  },
  [SET_DATA](state, payload) {
    state.data = payload;
  },
  [SET_DEALINGS](state, payload) {
    let ls = [];
    for (const item of payload) {
      item.count = parseInt(item.count);
      item.tenant_id = parseInt(item.tenant_id);
      item.main = bool(item.main);
      ls.push(item);
    }
    state.dealings = ls;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
