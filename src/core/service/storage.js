export const getCache = (target) => {
  return window.sessionStorage.getItem(target);
};

export const cacheItem = (target, item) => {
  window.sessionStorage.setItem(target, item);
};

export const getItem = (target) => {
  return window.localStorage.getItem(target);
};

export const saveItem = (target, item) => {
  window.localStorage.setItem(target, item);
};

export const destroyItem = (target) => {
  window.localStorage.removeItem(target);
};

export default { cacheItem, destroyItem, getItem, saveItem, destroyItem };
